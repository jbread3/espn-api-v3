* This code will break horribly at some point in the future due to changes in authentication or pages being moved or security updates
* This is intended to be only a basic demonstration of the initial flow of requests to obtain `SWID` and `espn_s2` cookies.
* A common error is `SSL certificate problem: self signed certificate in certificate chain`
  * [StackOverflow discussion](https://stackoverflow.com/questions/21187946/curl-error-60-ssl-certificate-issue-self-signed-certificate-in-certificate-cha)
