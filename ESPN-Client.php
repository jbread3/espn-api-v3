<?php
class ClientV3 {
  // The base of the URL, will be expanded once a season and league id are provided
  var $baseURL = "https://fantasy.espn.com/apis/v3/games/ffl/seasons/";
  
  // These only need to be changed if authentication fails
  var $apiKeyURL = "https://registerdisney.go.com/jgc/v6/client/ESPN-ONESITE.WEB-PROD/api-key?langPref=en-US";
  var $loginURL = "https://registerdisney.go.com/jgc/v6/client/ESPN-ONESITE.WEB-PROD/guest/login?langPref=en-US";
  
  var $seasonId = 2019;
  var $leagueId = -1;
  
  var $apiKey = "";
  var $swid = "";
  var $espns2 = "";
  
  function __construct($league_id, $season_id = 2019) {
    $this->leagueId = $league_id;
    $this->seasonId = $season_id;
  }
  
  // Two steps to get the desired SWID and ESPN_S2 cookie values:
  //  1) Get 'api-key' which is generated and given without supplying any information
  //  2) Perform a 'login' using your username and password (Connect) as well as the api-key
  function Connect($username = "", $password = "") {
    $postinfo = '{"loginValue" : "' . $username . '", "password" : "' . $password . '" }';
    $cookie_file_path = "cookie-espnv3.txt";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_URL, $this->apiKeyURL);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0',
      'content-type: application/json',
      'expires: -1'
    ));

    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
    curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, '');

    if (false === $apiKeyResponse = curl_exec($ch)) {
        echo 'Failure to retrieve API-Key. (' . curl_error($ch) . ')<br />';
        curl_close($ch);
        return;
    }
    
    $lines = explode("\r\n", $apiKeyResponse);
    curl_close($ch);

    foreach ($lines as $line) {
      $parts = explode(":", $line, 2);
      // Find the API-key
      if (trim($parts[0]) == "api-key") {
        $this->apiKey = trim($parts[1]);
      }
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_NOBODY, false);
    curl_setopt($ch, CURLOPT_URL, $this->loginURL);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0',
      'content-type: application/json',
      'expires: -1',
      'authorization: APIKEY ' . $this->apiKey
    ));

    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
    curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postinfo);
    $loginResponse = curl_exec($ch);

    $cookiesObj = json_decode($loginResponse);
    $this->swid = $cookiesObj->data->profile->swid;
    $this->espns2 = urldecode($cookiesObj->data->s2);
  }
  
  function GetTeams($season_id = 0) {
    if ($season_id <= 0)
      $season_id = $this->seasonId;
    $teamsURL = $this->baseURL . $season_id . "/segments/0/leagues/" . $this->leagueId . "?scoringPeriodId=1&view=mRoster&view=mTeam";
    
    // echo $teamsURL . '<br />';
    // echo $this->espns2 . ':' . $this->swid . '<br />';
        
    $teamCurlClient = curl_init();
    curl_setopt($teamCurlClient, CURLOPT_URL, $teamsURL);
    curl_setopt($teamCurlClient, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($teamCurlClient, CURLOPT_COOKIE, "espn_s2=" . $this->espns2 . ";SWID=" . $this->swid);
    if (false === $teamsResponse = curl_exec($teamCurlClient)) {
        echo 'Failure to retrieve team list. (' . curl_error($teamCurlClient) . ')<br />';
        curl_close($teamCurlClient);
        return false;
    }
    
    //echo $teamsResponse . '<br />';
    curl_close($teamCurlClient);

    return JSON_decode($teamsResponse);
  }
  
  function GetScores($weekId = 1) {
    if ($season_id <= 0)
      $season_id = $this->season_id;
    
    $scoresURL = $this->baseURL . $season_id . "/segments/0/leagues/" . $this->leagueId . "?view=mMatchup&view=mMatchupScore&scoringPeriodId=" . $weekId . "&matchupPeriodId=" . $weekId;
    
    $scoresCurlClient = curl_init();
    curl_setopt($scoresCurlClient, CURLOPT_URL, $scoresURL);
    curl_setopt($scoresCurlClient, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($scoresCurlClient, CURLOPT_COOKIE, "espn_s2=" . $this->espns2 . ";SWID=" . $this->swid);
    $scoresResponse = curl_exec($scoresCurlClient);
    curl_close($scoresCurlClient);
    
    return JSON_decode($scoresResponse);
  }
}
